import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Contact } from '../contact';
import { ApiService } from '../api.service';
import { MatTable } from '@angular/material/table';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  displayedColumns  :  string[] = ['id', 'name', 'title', 'email', 'phone', 'address', 'city', 'actions'];
  dataSource  = [];
  contact: Contact = {
    id: 0,
    name: "",
    title: "",
    email: "",
    phone: "",
    address:"",
    city: ""
  };
  @ViewChild(MatTable) table: MatTable<any>;

  constructor(private apiService: ApiService, private changeDetectorRefs: ChangeDetectorRef) { }

  ngOnInit(): void {    
    this.apiService.readContacts().subscribe((result)=>{   
      this.dataSource  =  result;
      this.table.renderRows();
     })
  }

  selectContact(contact){
    this.contact = contact;
  }

  newContact(){
    this.contact = {
      id: 0,
      name: "",
      title: "",
      email: "",
      phone: "",
      address:"",
      city: ""
    };
  }

  createContact(f){
    this.apiService.createContact(f.value).subscribe((result)=>{
      console.log(result);
      this.dataSource = [...this.dataSource, result]
      console.log(this.dataSource);
    });
    
  }

  deleteContact(id){
    this.apiService.deleteContact(id).subscribe((result: Contact)=>{
      console.log(result);
      this.dataSource = this.dataSource.filter((data)=>data.id !== result.id)
    });
  }

  updateContact(f){
    f.value.id = this.contact['id'];
    this.apiService.updateContact(f.value).subscribe((result)=>{
      console.log(result);
    });
  }


}
